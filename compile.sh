#!/bin/bash

# gfortran -g -O2 -DREALSIZE=8 -o demo-fortran demo-fortran.F /home/swift/Cuba-4.2/libcuba.a -lm
# gfortran -g -O2 -DREALSIZE=8 -o demo-fortran-2 demo-fortran-2.f90 /home/swift/Cuba-4.2/libcuba.a -lm

# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral.x collision_integral.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_2.x collision_integral_2.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_3.x collision_integral_3.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_5.x collision_integral_5.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_parabolic.x collision_integral_parabolic.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o denominator.x denominator.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm

# mpicc -o test.x test.c parse.c module.c -lm #hcubature.o -lm

# gcc -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o test.x test.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o test.x test.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o fermi_weight_test_2.x fermi_weight_test_2.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_Na_2.x collision_integral_Na_2.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_Na_3.x collision_integral_Na_3.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
# gcc -O3 -fomit-frame-pointer -Wall -DHAVE_CONFIG_H -DREALSIZE=8 -I/home/swift/Cuba-4.2/src/common -I. -I/home/swift/Cuba-4.2/src -o collision_integral_Na_4.x collision_integral_Na_4.c module.c parse.c find_mu.c /home/swift/Cuba-4.2/libcuba.a -lm
