/* Modules for calculating scattering rate and mobility */
#ifndef MODULE
#define MODULE

typedef struct{
	double val;
	double err;
} result;

typedef struct
{
	double x;
	double y;
	double z;
}vector;


// Squares a number
double Sq(double x);

/* Returns the magnitude of a vector with components x, y, z */
double vecmag(vector k);

/* Returns the sum of two vectors */
vector vecsum(vector k, vector q);

/* Returns the difference of two vectors */
vector vecdiff(vector k, vector q);

/* Returns the sum of two vectors */
vector vecscale(vector k, double scale);

/* Returns the dot product of two vectors */
double vecdot(vector k, vector q);

/* Returns the k-vector folded into (-0.5,0.5]^3*/
void BZfold(vector *k);

/* Returns the k-vector folded into [0, 1)^3 */
void BZfold_positive(vector *k);

void IBZfold(vector *k);

/* Energy based on an anlytic equation */
double Ek(int n, vector k);

// Velocity
vector velocity_k(int n, vector k);

/* Delta function */
double delta(double x);

/* Fermi-Dirac distribution function */
double fermi(double energy);

/* Derviative of the Fermi-Dirac distribution w.r.t energy */
double derv_fermi(double energy);

#endif
