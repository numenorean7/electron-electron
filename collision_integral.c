/*
  demo-c.c
    test program for the Cuba library
    last modified 13 Mar 15 th
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "parse.h"
#include "module.h"
#include "find_mu.h"
// #include <mpi.h>

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

double kappa_s_sq; // Screening length
vector uvec; //  u vector for calculating Phi
double EMAX; // Maximum energy for e2 integration
double prefactor; // Prefactor of the integral

typedef struct
{
  // integral_params: a type of struct which holds bands for the integrand.
  // In principle this could hold other parameters.
  int n1;
  int n2;
  int n3;
  int n4;
}integral_params;


double Ueff(int n1, vector k1, int n2, vector k2){
  // The effective Coulomb interaction between electrons
  // Explained in collision_integral.pdf

  vector qvec = vecdiff(k1, k2); // k1-k2
  double q = vecmag(qvec)*TPI/LATA; // magnitude of momentum transfer in A^-1

  double op_factor(void){
    // Finds the optical phonon factor

    int i, j;
    double num, denom;

    // LO phonon factor based on sum and products over modes
    // For more information, see collision_integral.pf section 1 b)

    double sum=0.0;
    for( i = 1; i <= ENLO[0]; i++){ // loop over polar LO mode only for scattering
      num = 1.0;
      denom = 1.0;
      for( j = 1; j <=ENTO[0]; j++){ // TO mode
        num = num*(1.0 - (ENTO[j]*ENTO[j])/(ENLO[i]*ENLO[i]) );
      }
      for( j = 1; j <=ENLO[0]; j++){ // polar LO mode
        if (j != i){
          denom = denom*(1.0 - (ENLO[j]*ENLO[j])/(ENLO[i]*ENLO[i]) );
        }
      }
      sum += fabs(num/denom); // For now, ignoring omega_{LO,i} / omega_{q,i}
  
    }

    // Put the prefactor and q dependance together with the sum to get the result.
    return -sum * ( FPI * ESQ * q*q ) / (EPSI * (q*q + kappa_s_sq)*(q*q + kappa_s_sq));
  }

  double U_C  = 0.0; // Set to zero unless we include them
  double U_op = 0.0; // Set to zero unless we include them
  double U_ap = 0.0; // Set to zero unless we include them

  // Coulomb factor
  if (INCLUDE_UC != 0) U_C = FPI * ESQ/( (q*q + kappa_s_sq) * EPSI); // eV A^3

  // Optical phonon factor
  if (INCLUDE_OP != 0) U_op = op_factor();

  // Acoustic phonon factor
  if (INCLUDE_AP != 0) U_ap = -0.477881 * E_D * E_D; // eV A^3
  
  return U_C + U_op + U_ap;

}

static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *userdata) {

  // Integrand for the main collision integral

  // Get k1 and k3
  vector k1 = {xx[0]-0.5, xx[1]-0.5, xx[2]-0.5};
  vector k3 = {xx[3]-0.5, xx[4]-0.5, xx[5]-0.5};

  // Import the bands
  integral_params *bands = (integral_params*)userdata;
  int n1 = bands->n1;
  int n2 = bands->n2;
  int n3 = bands->n3;
  // int n4 = bands->n4;

  // Calculate the energies
  double e1 = Ek(n1,k1);
  double e2 = xx[6]*EMAX;
  double e3 = Ek(n3,k3);
  double e4 = e1+e2-e3;

  // If e4 is negative the delta function can't be satisfied.
  if(e4<0){
    ff[0] = 0.0;
    return 0;
  }

  double Pmin = fabs(sqrt(2.0*e2)-sqrt(2.0*e4));
  double Pmax = sqrt(2.0*e2)+sqrt(2.0*e4);
  vector Q, p1, p3;
  p1.x = HBAR/sqrt(M[n1].x)*k1.x;
  p1.y = HBAR/sqrt(M[n1].y)*k1.y;
  p1.z = HBAR/sqrt(M[n1].z)*k1.z;
  p3.x = HBAR/sqrt(M[n3].x)*k3.x;
  p3.y = HBAR/sqrt(M[n3].y)*k3.y;
  p3.z = HBAR/sqrt(M[n3].z)*k3.z;
  Q.x = sqrt(M[n1].x / M[n2].x) * (p1.x - p3.x);
  Q.y = sqrt(M[n1].y / M[n2].y) * (p1.y - p3.y);
  Q.z = sqrt(M[n1].z / M[n2].z) * (p1.z - p3.z);

  // Implement the theta function
  double Qmag = vecmag(Q);
  if (Qmag <= Pmin || Pmax <= Qmag){
    ff[0] = 0.0;
    return 0;
  }

  // U factor:
  double U = Ueff(n1, k1, n3, k3);

  // Fermi weights:
  double fweights = fermi(e1)*fermi(e2)*(1.0-fermi(e3))*(1.0-fermi(e4));

  // Cosine factor
  double cosfactor = p1.z - p3.z;

  // The result of the integration:
  ff[0] = prefactor * EMAX * Sq(U) * fweights * Sq(cosfactor) / Qmag;

  if (ff[0]!=ff[0]){
    printf("prefactor: %e \tEMAX: %e \tU: %e\nfweights:  %e \tcosfactor %e \tQmag: %e\n", 
      prefactor , EMAX, U, fweights, cosfactor, Qmag);
    printf("ff: %e\n", ff[0]);
  }
  // Units: Phi^2 * eV^2 A^6 * 1 * eV^-1 = eV Phi^2 A^6

  return 0;
}

/*********************************************************************/

// Parameters for the Cuba integration

#define NDIM 7
#define NCOMP 1
#define NVEC 1
#define EPSREL 1e-3
#define EPSABS 1e-12
// #define VERBOSE 1
#define LAST 4
#define SEED 0
#define MINEVAL 0
// #define MAXEVAL 1e10

// #define NSTART 1000
// #define NINCREASE 500
// #define NBATCH 1000
// #define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

// #define NNEW 1000
// #define NMIN 2
// #define FLATNESS 25.

#define KEY1 1e4
#define KEY2 5
#define KEY3 1
#define MAXPASS 10
#define BORDER 0.
#define MAXCHISQ 10.
#define MINDEVIATION .25
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0


int main( int argc, char *argv[] ) {

  printf("++++++++++++++++++++++++++++++++++++++++\n");
  printf("+ Electron-Electron Collision Integral +\n");
  printf("+            Michael Swift             +\n");
  printf("+        Karthik Krishnaswamy          +\n");
  printf("+          Burak Himmetoglu            +\n");
  printf("++++++++++++++++++++++++++++++++++++++++\n\n");
  char input_file[] = "INPUT++++++++++++";
  if (argc >= 2){
    strcpy(input_file, argv[1]);
  }
  else{
    strcpy(input_file, "INPUT");
  }
  printf("Reading input file %s \n", input_file);
  parse_input(input_file);
  printf("Input parameters:\n");
  printf("Temperature %f K\n", T);
  printf("Electron density %e cm^-3\n", ELDEN);
  printf("Epsilon infinity %f\n", EPSI);
  printf("Deformation potential %f\n", E_D);
  printf("Delta function smearing %f eV\n", SMEAR);
  printf("Lattice parameter %f A\n", LATA);
  uvec.x = UVEC[0];
  uvec.y = UVEC[1];
  uvec.z = UVEC[2];
  printf("u vector for calculating Phi: (%f, %f, %f)\n", uvec.x, uvec.y, uvec.z);
  printf("TO Phonon Modes:\n");
  int i;
  for( i = 1; i <= ENTO[0]; i++){
    printf("    %f \n", ENTO[i]);}
  printf("LO Phonon Modes:\n");
  for( i = 1; i <= ENLO[0]; i++){
    printf("    %f \n", ENLO[i]);}

  if (PARABOLIC_BANDS == 1){
    printf("Using parabolic bands.\n");
  }
  else{
    printf("Using an interpolated first-principles band structure.\n");
    printf("WARNING: parabolic bands assumed in building integrand.\n");
    read_cbands();
    read_IBZmap();
  }

  printf("Integral includes:\n");
  if (INCLUDE_UC != 0){
    printf("    Coulomb interaction\n");
  }
  if (INCLUDE_OP != 0){
    printf("    LO phonon-mediated interaction\n");
  }
  if (INCLUDE_AP != 0){
    printf("    Acoustic phonon-mediated interaction\n");
  }

  integral_params bands = {BANDS[0], BANDS[1], BANDS[2], BANDS[3]};
  integral_params *userdata = &bands;

  // Set the masses
  M[0].x = 2.0*HBAR*HBAR / (LATA*LATA*T_DELTA); // eV s^2 A^-2
  M[0].y = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[0].z = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  
  M[1].x = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[1].y = 2.0*HBAR*HBAR / (LATA*LATA*T_DELTA); // eV s^2 A^-2
  M[1].z = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
    
  M[2].x = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[2].y = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[2].z = 2.0*HBAR*HBAR / (LATA*LATA*T_DELTA); // eV s^2 A^-2

  vector kz={0.0, 0.0, 0.5};
  EMAX = Ek(0,kz);

  printf("Finding chemical potential\n");
  MU = find_mu(ELDEN,&kappa_s_sq);

  // Symbolic prefactor of the integral
  prefactor = ( sqrt(M[0].x*M[0].y*M[0].z) ) / (TPI * K_B * T * pow(HBAR,4) * pow(LATA,6));

  // Mass prefactor
  double massfactor = Sq((1.0/M[BANDS[0]].z - 1.0/M[BANDS[1]].z))*M[BANDS[0]].z;
  // Full prefactor
  prefactor = prefactor * massfactor;
  printf("prefactor: %e eV^-1/2 s^-3 A^-7", prefactor);


  printf("\nPerforming collision integral for bands (%i, %i, %i, %i).\n", bands.n1, bands.n2, bands.n3, bands.n4);

  int comp, nregions, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  printf("\n------------------- Divonne -------------------\n");

  long long int MAXEVAL = 1e10;
  long long int neval;

  llDivonne(NDIM, NCOMP, Integrand, userdata, NVEC,
    EPSREL, EPSABS, VERBOSE, SEED,
    MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
    BORDER, MAXCHISQ, MINDEVIATION,
    NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
    STATEFILE, SPIN,
    &nregions, &neval, &fail, integral, error, prob);

  printf("DIVONNE RESULT:\tnregions %d\tneval %lld\tfail %d\n",
    nregions, neval, fail);
  for( comp = 0; comp < NCOMP; ++comp )
    printf("DIVONNE RESULT:\t%.8e +- %.8e eV^-1 s^-3 A^-1\tp = %.3f\n",
      (double)integral[comp], (double)error[comp], (double)prob[comp]);

  return 0;
}
