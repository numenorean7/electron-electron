/*
  demo-c.c
    test program for the Cuba library
    last modified 13 Mar 15 th
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "parse.h"
#include "module.h"
#include "find_mu.h"
// #include <mpi.h>

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

double kappa_s_sq; // Screening length
double prefactor; // Prefactor of the integral
double m; // mass
vector G; // Umklapp vector

typedef struct
{
  // integral_params: a type of struct which holds bands for the integrand.
  // In principle this could hold other parameters.
  int n1;
  int n2;
  int n3;
  int n4;
}integral_params;


double Ueff(int n1, vector k1, int n2, vector k2){
  // The effective Coulomb interaction between electrons
  // Explained in collision_integral.pdf

  vector qvec = vecdiff(k1, k2); // k1-k2
  double q = vecmag(qvec)*TPI/LATA; // magnitude of momentum transfer in A^-1

  double op_factor(void){
    // Finds the optical phonon factor

    int i, j;
    double num, denom;

    // LO phonon factor based on sum and products over modes
    // For more information, see collision_integral.pf section 1 b)

    double sum=0.0;
    for( i = 1; i <= ENLO[0]; i++){ // loop over polar LO mode only for scattering
      num = 1.0;
      denom = 1.0;
      for( j = 1; j <=ENTO[0]; j++){ // TO mode
        num = num*(1.0 - (ENTO[j]*ENTO[j])/(ENLO[i]*ENLO[i]) );
      }
      for( j = 1; j <=ENLO[0]; j++){ // polar LO mode
        if (j != i){
          denom = denom*(1.0 - (ENLO[j]*ENLO[j])/(ENLO[i]*ENLO[i]) );
        }
      }
      sum += fabs(num/denom); // For now, ignoring omega_{LO,i} / omega_{q,i}
  
    }

    // Put the prefactor and q dependance together with the sum to get the result.
    return -sum * ( FPI * ESQ * q*q ) / (EPSI * (q*q + kappa_s_sq)*(q*q + kappa_s_sq));
  }

  double U_C  = 0.0; // Set to zero unless we include them
  double U_op = 0.0; // Set to zero unless we include them
  double U_ap = 0.0; // Set to zero unless we include them

  // Coulomb factor
  if (INCLUDE_UC != 0) U_C = FPI * ESQ/( (q*q + kappa_s_sq) * EPSI); // eV A^3

  // Optical phonon factor
  if (INCLUDE_OP != 0) U_op = op_factor();

  // Acoustic phonon factor
  if (INCLUDE_AP != 0) U_ap = -0.477881 * E_D * E_D; // eV A^3
  
  return U_C + U_op + U_ap;

}

static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *userdata) {

  vector k1 = {xx[0]-0.5, xx[1]-0.5, xx[2]-0.5};
  vector k3 = {xx[3]-0.5, xx[4]-0.5, xx[5]-0.5};
  double k2mag = fabs(xx[6]-0.5);
  vector k2dummy = {xx[6]-0.5, 0.0, 0.0};

  double e1 = Ek(0,k1);
  double e2 = Ek(0,k2dummy);
  double e3 = Ek(0,k3);
  double e4 = e1 + e2 - e3;

  if (e4<=0){
    ff[0] = 0.0;;
    return 0;
  }

  double k4mag = sqrt(2.0*m*e4)/HBAR;

  // The result of the integration:
  double fermi_factor = fermi(e1)*fermi(e2)*(1.0-fermi(e3))*(1.0-fermi(e4));

  if (PMU_APPROX == 1){
    double k_Fermi = sqrt(2.0*m*MU)/HBAR;
    k1 = vecscale(k1, k_Fermi/vecmag(k1));
    k2mag = k_Fermi;
    k3 = vecscale(k3, k_Fermi/vecmag(k3));
    k4mag = k_Fermi;
  }

  vector Q = vecdiff( vecdiff(k1,k3), G);
  double Qmag = vecmag(Q);
  double Kmin = fabs(k2mag - k4mag);
  double Kmax = k2mag+k4mag;

  // Implement the theta function
  if (Qmag <= Kmin || Kmax <= Qmag){
    ff[0] = 0.0;
    return 0;
  }

  // U factor:
  double U = Ueff(0, k1, 0, k3);

  // The result of the integration:
  ff[0] = prefactor * Sq(U) * fermi_factor * k2mag/ Qmag ;


  if (ff[0]!=ff[0]){
    printf("prefactor: %e fermi factor: %e \tU: %e \tk2mag %e \tQmag: %e\n", 
      prefactor , fermi_factor, U, k2mag, Qmag);
    printf("ff: %e\n", ff[0]);
  }
  // Units: Phi^2 * eV^2 A^6 * 1 * eV^-1 = eV Phi^2 A^6

  return 0;
}




/*********************************************************************/

// Parameters for the Cuba integration

// #define NDIM 4
#define NCOMP 1
#define NVEC 1
#define EPSREL 1e-3
#define EPSABS 1e-16
// #define VERBOSE 1
#define LAST 4
#define SEED 0
#define MINEVAL 0
// #define MAXEVAL 1e10

// #define NSTART 1000
// #define NINCREASE 500
// #define NBATCH 1000
// #define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

// #define NNEW 1000
// #define NMIN 2
// #define FLATNESS 25.

#define KEY1 1e4
#define KEY2 5
#define KEY3 1
#define MAXPASS 10
#define BORDER 0.
#define MAXCHISQ 10.
#define MINDEVIATION .25
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0


int main( int argc, char *argv[] ) {

  printf("++++++++++++++++++++++++++++++++++++++++\n");
  printf("+ Electron-Electron Collision Integral +\n");
  printf("+            Michael Swift             +\n");
  printf("+        Karthik Krishnaswamy          +\n");
  printf("+          Burak Himmetoglu            +\n");
  printf("++++++++++++++++++++++++++++++++++++++++\n\n");
  char input_file[] = "INPUT++++++++++++";
  if (argc >= 2){
    strcpy(input_file, argv[1]);
  }
  else{
    strcpy(input_file, "INPUT");
  }
  printf("Reading input file %s \n", input_file);
  parse_input(input_file);
  printf("Input parameters:\n");
  printf("Temperature %f K\n", T);
  printf("Electron density %e cm^-3\n", ELDEN);
  printf("Epsilon infinity %f\n", EPSI);
  printf("Deformation potential %f\n", E_D);
  printf("Lattice parameter %f A\n", LATA);
  printf("TO Phonon Modes:\n");
  int i;
  for( i = 1; i <= ENTO[0]; i++){
    printf("    %f \n", ENTO[i]);}
  printf("LO Phonon Modes:\n");
  for( i = 1; i <= ENLO[0]; i++){
    printf("    %f \n", ENLO[i]);}

  if (PARABOLIC_BANDS == 1){
    printf("Using parabolic bands.\n");
  }
  else{
    printf("Using an interpolated first-principles band structure.\n");
    printf("WARNING: parabolic bands assumed in building integrand.\n");
    read_cbands();
    read_IBZmap();
  }

  printf("Integral includes:\n");
  if (INCLUDE_UC != 0){
    printf("    Coulomb interaction\n");
  }
  if (INCLUDE_OP != 0){
    printf("    LO phonon-mediated interaction\n");
  }
  if (INCLUDE_AP != 0){
    printf("    Acoustic phonon-mediated interaction\n");
  }

  for(i=0;i<4;i++){
    if(BANDS[i]<0 || NBANDS<BANDS[i]){
      printf("WARNING: BANDS[%i]=%i is out of range!\n",i,BANDS[i]);
    }
  }
  integral_params bands = {BANDS[0], BANDS[1], BANDS[2], BANDS[3]};
  integral_params *userdata = &bands;

  G.x = UMKLAPP[0];
  G.y = UMKLAPP[1];
  G.z = UMKLAPP[2];

  // Set the mass
  m = 2.0*HBAR*HBAR * PI*PI / (LATA*LATA*T_PI); // eV s^2 A^-2
  printf("Mass: %e eV s^2 A^-2 \n", m);
  M[0].x = m; 
  M[0].y = m;
  M[0].z = m;

  printf("Finding chemical potential\n");
  MU = find_mu(ELDEN, &kappa_s_sq);

  // Prefactor of integral 
  prefactor = 8.98755e17 * 9.0 * pow(PI,6) * pow(HBAR,5) * Sq(G.z) /
    (4.0 * ESQ * MU*MU*MU * m*m * K_B*T * pow(LATA,9));
  // 8.98755e17 converts seconds to microOhm centimeters
  
  // double result=0;
  // double result_err=0;

  // for (G.x =-1; G.x < 2; G.x++){
  //   for (G.y =-1; G.y < 2; G.y++){
  //     for (G.z =-1; G.z < 2; G.z+=2)

  printf("\nPerforming collision integral for G = (%f, %f, %f).\n", G.x, G.y, G.z);

  int comp, nregions, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  printf("\n------------------- Divonne -------------------\n");

  long long int MAXEVAL = 1e10;
  long long int neval;
  int NDIM = 7;

  llDivonne(NDIM, NCOMP, Integrand, userdata, NVEC,
    EPSREL, EPSABS, VERBOSE, SEED,
    MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
    BORDER, MAXCHISQ, MINDEVIATION,
    NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
    STATEFILE, SPIN,
    &nregions, &neval, &fail, integral, error, prob);

  printf("DIVONNE RESULT:\tnregions %d\tneval %lld\tfail %d\n",
    nregions, neval, fail);
  for( comp = 0; comp < NCOMP; ++comp )
    printf("RESULT:\t%.8e +- %.8e μΩ cm \tp = %.3f\n",
      (double)integral[comp], (double)error[comp], (double)prob[comp]);

  // result += integral[0];
  // result_err += error[0];

  // printf("Final result: %e +- %e s\n", result, result_err);


  return 0;
}
