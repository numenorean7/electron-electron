/*
  demo-c.c
    test program for the Cuba library
    last modified 13 Mar 15 th
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "parse.h"
#include "module.h"
#include "find_mu.h"
// #include <mpi.h>

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

double kappa_s_sq; // Screening length
vector uvec; //  u vector for calculating Phi
double emin, emax, escale;
double prefactor; // Prefactor of the integral

typedef struct
{
  // integral_params: a type of struct which holds bands for the integrand.
  // In principle this could hold other parameters.
  int n1;
  int n2;
  int n3;
  int n4;
}integral_params;


double Ueff(int n1, vector k1, int n2, vector k2){
  // The effective Coulomb interaction between electrons
  // Explained in collision_integral.pdf

  vector qvec = vecdiff(k1, k2); // k1-k2
  double q = vecmag(qvec)*TPI/LATA; // magnitude of momentum transfer in A^-1

  double op_factor(void){
    // Finds the optical phonon factor

    int i, j;
    double num, denom;

    // LO phonon factor based on sum and products over modes
    // For more information, see collision_integral.pf section 1 b)

    double sum=0.0;
    for( i = 1; i <= ENLO[0]; i++){ // loop over polar LO mode only for scattering
      num = 1.0;
      denom = 1.0;
      for( j = 1; j <=ENTO[0]; j++){ // TO mode
        num = num*(1.0 - (ENTO[j]*ENTO[j])/(ENLO[i]*ENLO[i]) );
      }
      for( j = 1; j <=ENLO[0]; j++){ // polar LO mode
        if (j != i){
          denom = denom*(1.0 - (ENLO[j]*ENLO[j])/(ENLO[i]*ENLO[i]) );
        }
      }
      sum += fabs(num/denom); // For now, ignoring omega_{LO,i} / omega_{q,i}
  
    }

    // Put the prefactor and q dependance together with the sum to get the result.
    return -sum * ( FPI * ESQ * q*q ) / (EPSI * (q*q + kappa_s_sq)*(q*q + kappa_s_sq));
  }

  double U_C  = 0.0; // Set to zero unless we include them
  double U_op = 0.0; // Set to zero unless we include them
  double U_ap = 0.0; // Set to zero unless we include them

  // Coulomb factor
  if (INCLUDE_UC != 0) U_C = FPI * ESQ/( (q*q + kappa_s_sq) * EPSI); // eV A^3

  // Optical phonon factor
  if (INCLUDE_OP != 0) U_op = op_factor();

  // Acoustic phonon factor
  if (INCLUDE_AP != 0) U_ap = -0.477881 * E_D * E_D; // eV A^3
  
  return U_C + U_op + U_ap;

}

static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *userdata) {

  // The Fermi function
  double e1 = xx[4]*escale + emin;
  double e2 = xx[5]*escale + emin;
  double e3 = xx[6]*escale + emin;
  double e4 = e1+e2-e3;

  if (e4 < 0){
    ff[0] = 0.0;
    return 0;
  }

  double fermi_factor = fermi(e1)*fermi(e2)*(1.0-fermi(e3))*(1.0-fermi(e4));

  double p1mag, p2mag, p3mag, p4mag;
  if (PMU_APPROX == 1){
    p1mag = sqrt(2.0*MU);
    p2mag = sqrt(2.0*MU);
    p3mag = sqrt(2.0*MU);
    p4mag = sqrt(2.0*MU);
  }
  else{
    p1mag = sqrt(2.0*e1);
    p2mag = sqrt(2.0*e2);
    p3mag = sqrt(2.0*e3);
    p4mag = sqrt(2.0*e4);
  }

  // Integrand for the main collision integral

  double theta1 = PI*xx[0];
  double theta3 = PI*xx[1];
  double phi1 = TPI*xx[2];
  double phi3 = TPI*xx[3];
  double measure_factor = escale*escale*escale * p1mag * p3mag 
     * sin(theta1) * sin(theta3) * PI*PI * TPI*TPI;


  // Get p1 and p3
  vector p1 = {p1mag*sin(theta1)*cos(phi1), p1mag*sin(theta1)*sin(phi1), p1mag*cos(theta1)};
  vector p3 = {p3mag*sin(theta3)*cos(phi3), p3mag*sin(theta3)*sin(phi3), p3mag*cos(theta3)};

  // Import the bands
  integral_params *bands = (integral_params*)userdata;
  int n1 = bands->n1;
  int n2 = bands->n2;
  int n3 = bands->n3;
  // int n4 = bands->n4;

  double Pmin = fabs(p2mag-p4mag);
  double Pmax = p2mag+p4mag;
  vector Q, k1, k3;
  k1.x = p1.x * sqrt(M[n1].x) / HBAR  *  LATA / TPI;
  k1.y = p1.y * sqrt(M[n1].y) / HBAR  *  LATA / TPI;
  k1.z = p1.z * sqrt(M[n1].z) / HBAR  *  LATA / TPI;
  k3.x = p3.x * sqrt(M[n3].x) / HBAR  *  LATA / TPI;
  k3.y = p3.y * sqrt(M[n3].y) / HBAR  *  LATA / TPI;
  k3.z = p3.z * sqrt(M[n3].z) / HBAR  *  LATA / TPI;
  Q.x = sqrt(M[n1].x / M[n2].x) * (p1.x - p3.x);
  Q.y = sqrt(M[n1].y / M[n2].y) * (p1.y - p3.y);
  Q.z = sqrt(M[n1].z / M[n2].z) * (p1.z - p3.z);
  double Qmag = vecmag(Q);

  // Implement the theta function
  if (Qmag <= Pmin || Pmax <= Qmag){
    ff[0] = 0.0;
    return 0;
  }

  // U factor:
  double U = Ueff(n1, k1, n3, k3);

  // Cosine factor
  double cosfactor = p1mag*cos(theta1) - p3mag*cos(theta3);

  // The result of the integration:
  ff[0] = prefactor *  measure_factor * fermi_factor * Sq(U) * Sq(cosfactor) / Qmag;

  if (ff[0]!=ff[0]){
    printf("prefactor: %e Measure: %e \tU: %e\ncosfactor %e \tQmag: %e\n", 
      prefactor , measure_factor, U, cosfactor, Qmag);
    printf("ff: %e\n", ff[0]);
  }
  // Units: Phi^2 * eV^2 A^6 * 1 * eV^-1 = eV Phi^2 A^6

  return 0;
}

static int Integrand_denom(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *userdata) {

  vector k = {xx[0]-0.5, xx[1]-0.5, xx[2]-0.5};
  ff[0] = 0.0;
  ff[1] = 0.0;
  ff[2] = 0.0;
  vector uvec = {0.0, 0.0, 1.0};

  int n;
  for (n=0;n<NBANDS;n++){
    double e = Ek(n,k);
    vector v = velocity_k(n,k);
    double Phi = vecdot(v,uvec);
    double dfde = derv_fermi(e);
  
    ff[0] += v.x * Phi * dfde;
    ff[1] += v.y * Phi * dfde;
    ff[2] += v.z * Phi * dfde;
  }

  return 0;

}


/*********************************************************************/

// Parameters for the Cuba integration

// #define NDIM 4
// #define NCOMP 1
#define NVEC 1
#define EPSREL 1e-3
#define EPSABS 1e-12
// #define VERBOSE 1
#define LAST 4
#define SEED 0
#define MINEVAL 0
// #define MAXEVAL 1e10

// #define NSTART 1000
// #define NINCREASE 500
// #define NBATCH 1000
// #define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

// #define NNEW 1000
// #define NMIN 2
// #define FLATNESS 25.

#define KEY1 1e4
#define KEY2 5
#define KEY3 1
#define MAXPASS 10
#define BORDER 0.
#define MAXCHISQ 10.
#define MINDEVIATION .25
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0


int main( int argc, char *argv[] ) {

  printf("++++++++++++++++++++++++++++++++++++++++\n");
  printf("+ Electron-Electron Collision Integral +\n");
  printf("+            Michael Swift             +\n");
  printf("+        Karthik Krishnaswamy          +\n");
  printf("+          Burak Himmetoglu            +\n");
  printf("++++++++++++++++++++++++++++++++++++++++\n\n");
  char input_file[] = "INPUT++++++++++++";
  if (argc >= 2){
    strcpy(input_file, argv[1]);
  }
  else{
    strcpy(input_file, "INPUT");
  }
  printf("Reading input file %s \n", input_file);
  parse_input(input_file);
  printf("Input parameters:\n");
  printf("Temperature %f K\n", T);
  printf("Electron density %e cm^-3\n", ELDEN);
  printf("Epsilon infinity %f\n", EPSI);
  printf("Deformation potential %f\n", E_D);
  printf("Lattice parameter %f A\n", LATA);
  uvec.x = UVEC[0];
  uvec.y = UVEC[1];
  uvec.z = UVEC[2];
  printf("u vector for calculating Phi: (%f, %f, %f)\n", uvec.x, uvec.y, uvec.z);
  printf("TO Phonon Modes:\n");
  int i;
  for( i = 1; i <= ENTO[0]; i++){
    printf("    %f \n", ENTO[i]);}
  printf("LO Phonon Modes:\n");
  for( i = 1; i <= ENLO[0]; i++){
    printf("    %f \n", ENLO[i]);}

  if (PARABOLIC_BANDS == 1){
    printf("Using parabolic bands.\n");
  }
  else{
    printf("Using an interpolated first-principles band structure.\n");
    printf("WARNING: parabolic bands assumed in building integrand.\n");
    read_cbands();
    read_IBZmap();
  }

  printf("Integral includes:\n");
  if (INCLUDE_UC != 0){
    printf("    Coulomb interaction\n");
  }
  if (INCLUDE_OP != 0){
    printf("    LO phonon-mediated interaction\n");
  }
  if (INCLUDE_AP != 0){
    printf("    Acoustic phonon-mediated interaction\n");
  }

  for(i=0;i<4;i++){
    if(BANDS[i]<0 || NBANDS<BANDS[i]){
      printf("WARNING: BANDS[%i]=%i is out of range!\n",i,BANDS[i]);
    }
  }
  integral_params bands = {BANDS[0], BANDS[1], BANDS[2], BANDS[3]};
  integral_params *userdata = &bands;

  // Set the masses
  M[0].x = 2.0*HBAR*HBAR / (LATA*LATA*T_DELTA); // eV s^2 A^-2
  M[0].y = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[0].z = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  
  M[1].x = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[1].y = 2.0*HBAR*HBAR / (LATA*LATA*T_DELTA); // eV s^2 A^-2
  M[1].z = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
    
  M[2].x = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[2].y = 2.0*HBAR*HBAR / (LATA*LATA*T_PI); // eV s^2 A^-2
  M[2].z = 2.0*HBAR*HBAR / (LATA*LATA*T_DELTA); // eV s^2 A^-2


  printf("Finding chemical potential\n");
  MU = find_mu(ELDEN, &kappa_s_sq);

  emin = MU - 100*K_B*T;
  emin = ((emin < 0) ? (0.0) : (emin));
  emax = MU + 100*K_B*T;
  emax = ((EMAX < emax) ? (EMAX) : (emax));
  escale = emax - emin;
  printf("Integrating from %f eV to %f eV", emin, emax);

  // Symbolic prefactor of the integral
  double mDcubed = M[0].x*M[0].y*M[0].z;
  // double mb = 3.0 * 1.0/(1.0/M[0].x + 2.0/M[0].y);
  // prefactor = 1e3 * Sq(BOLTZMANNC) * PI/192.0 * ELDEN*1.0e-24/Sq(FPI) * 
  //   sqrt(mDcubed)*mb/pow(HBAR*MU,3);
  prefactor = 8.98755e17 * pow(mDcubed,1.5)/(2.0 * K_B*T * pow(TPI,7) * pow(HBAR,10)); 

  // 8.98755e17 converts seconds to microOhm centimeters

  // Mass prefactor
  double massfactor = Sq((1.0/M[BANDS[0]].z - 1.0/M[BANDS[1]].z))*M[BANDS[0]].z;
  // Full prefactor
  prefactor = prefactor * massfactor;
  printf("prefactor: %e ", prefactor);


  printf("\nPerforming collision integral for bands (%i, %i, %i, %i).\n", bands.n1, bands.n2, bands.n3, bands.n4);

  int comp, nregions, fail;
  int NCOMP = 3;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  printf("\n------------------- Divonne -------------------\n");

  long long int MAXEVAL = 2e10;
  long long int neval;
  int NDIM = 7;
  NCOMP = 1;

  llDivonne(NDIM, NCOMP, Integrand, userdata, NVEC,
    EPSREL, EPSABS, VERBOSE, SEED,
    MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
    BORDER, MAXCHISQ, MINDEVIATION,
    NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
    STATEFILE, SPIN,
    &nregions, &neval, &fail, integral, error, prob);

  printf("DIVONNE RESULT:\tnregions %d\tneval %lld\tfail %d\n",
    nregions, neval, fail);
  for( comp = 0; comp < NCOMP; ++comp )
    printf("DIVONNE RESULT:\t%.8e +- %.8e \tp = %.3f\n",
      (double)integral[comp], (double)error[comp], (double)prob[comp]);

  double coll_int = integral[0];
  double coll_err = error[0];
  double denom;
  double denom_err;

  if (ZERO_T_EF == 1){
    double mb = 3.0 * 1.0/(1.0/M[0].x + 2.0/M[0].y);
    denom = ESQ * 8 * MU*MU*MU * mDcubed /
     (Sq(PI*PI * HBAR*HBAR*HBAR * mb));
    printf("Analytic Denominator:\t%.8e\n", denom);
    denom_err = 0;
  }
  else{

    printf("\n------------------- Divonne -------------------\n");

    NDIM = 3;
    NCOMP = 3;

    llDivonne(NDIM, NCOMP, Integrand_denom, userdata, NVEC,
      EPSREL, EPSABS, VERBOSE, SEED,
      MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
      BORDER, MAXCHISQ, MINDEVIATION,
      NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
      STATEFILE, SPIN,
      &nregions, &neval, &fail, integral, error, prob);

    printf("DIVONNE RESULT:\tnregions %d\tneval %lld\tfail %d\n",
      nregions, neval, fail);
    for( comp = 0; comp < NCOMP; ++comp )
      printf("DIVONNE RESULT:\t%.8e +- %.8e \tp = %.3f\n",
        (double)integral[comp], (double)error[comp], (double)prob[comp]);

    denom = 4.0*ESQ/pow(LATA,6) *
       (Sq(integral[0]) + Sq(integral[1]) + Sq(integral[2]));
    denom_err = 4.0*ESQ/pow(LATA,6) *
       sqrt(Sq(2*integral[0])*Sq(error[0]) + Sq(2*integral[1])*Sq(error[1]) + Sq(2*integral[2])*Sq(error[2]) );
    printf("Numerical Denominator:\t%.8e +- %.8e\n", denom, denom_err);
  }

  double result = coll_int / denom;
  double result_err = sqrt( Sq(coll_err/denom) + Sq(coll_int/Sq(denom) * denom_err) );

  printf("Final RESULT:\t%.8e +- %.8e μΩ cm\n", result, result_err);

  return 0;
}
