#ifndef PARSE
#define PARSE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include "module.h"

/* CONSTANTS USED */

#define NOTSET -1e30     // some floating point number

#define PI 3.141592653589793 // pi
#define TPI 6.28318530718 // 2pi
#define FPI 12.566370614 // 4pi
#define HBAR 6.582119514e-16 // eV s
#define K_B 8.6173324e-5 // eV K^-1
#define BOLTZMANNC 8.6173324e-5 // eV K^-1
#define ESQ 14.3996433 // eV A
//#define eVHart 27.211

/* ** PARAMETERS WITH DEFAULT VALUES ** */
extern double MASS; // effective mass
extern double T;
extern double LATA; //lattice parameter
extern double MU; // chemical potential
extern double MU_LBOUND;
extern double MU_UBOUND;
extern double EMAX; // max energy for energy integration
extern double ELDEN; // electron density in cm-3
extern double EPSI; // epsilon_infinity
extern double EPS; // epsilon_static
extern double E_D; // Deformation potential

extern double QSCR; // epsilon_static
extern double NDERVMU; // epsilon_static

extern double SMEAR; // smearing for the delta function

extern int PARABOLIC_BANDS; // whether or not to use parabolic bands.
extern double T_PI;
extern double T_DELTA;
extern double SHIFTS[10]; // Shifts of the minimum of the potential
extern int ZERO_T_EF; // whether or not to use Klimin's expressions for mu and kappa_s_sq.
extern int PMU_APPROX; // Whether to approximate p = p_mu

extern char CBANDS_FILE[];
extern char EQKFIT_FILE[];
extern char VKFIT_FILE[];
extern int NBANDS;
extern int BANDS[4];
extern double UMKLAPP[3];
extern int NKIBZ;

extern vector M[10];

extern int METHOD_FIND_MU;

extern int VERBOSE;
extern int VERBOSE_FIND_MU;

extern int INCLUDE_UC;
extern int INCLUDE_OP;
extern int INCLUDE_AP;

extern double TAUADD; // to include additional scattering mechanisms; Matthiessen's rule
extern double TAUOUT; // to print out tau at kz=TAUOUT
extern double TAUCONST; // to use constant tau for el-ph

extern double ETAOUT; // to print out eta, dimensionless parameter used in imp calcs at kz=ETAOUT

extern int NSIG; // number of grid points along x,y,z for sigma integration
extern int NTAU; // number of grid points along x,y,z for tau integration
extern int NTAUSPH; // number of grid points in spherical for tau integration
extern int NINTERP; // number of grid points along x,y,z for interpolation
extern int NKBZ;

extern double IMPDEN[10*sizeof(double)]; // density of defects in cm-3
extern double IMPZ[10*sizeof(double)]; // charge of impurity defects; 0 indicates neutral defect

extern double ENTO[10*sizeof(double)]; // TO mode frequencies in cm-1
extern double ENLO[10*sizeof(double)]; // LO mode frequencies in cm-1
// extern double UVEC[4*sizeof(double)]; // u vector for computing Phi = v dot u
extern double UVEC[3];


extern double **cbands;
extern int *IBZmap;


void read_cbands(void);
void read_IBZmap(void);
void free_cbands(void);
// void read_cbands(double cbands[NBANDS][NKIBZ]);
// void read_IBZmap(int IBZmap[NKBZ]);


char *trimspace(char *str);

char * stripcomments(char *str);

char * get_key(char *str);

char * get_args(char *str);

int lookup(char *key, char *args);

int parse_input(char *fname);

#endif
