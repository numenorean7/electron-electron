/* Modules for calculating scattering rate and mobility
 * Written by Karthik K. Oct 2015
 * Heavily modified by Michael S
 * Depends on Cuba. */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "parse.h"
#include "module.h"
// #include <mpi.h>

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

double Sq(double x) {
  return x*x;
}

double vecmag(vector k){
	/* Returns the magnitude of a vector with components x, y, z */

	return sqrt(Sq(k.x) + Sq(k.y) + Sq(k.z)) ;
}

vector vecsum(vector k, vector q){
	/* Returns the sum of two vectors */
	vector sum;
	sum.x = k.x + q.x;
	sum.y = k.y + q.y;
	sum.z = k.z + q.z;

	return sum;
}

vector vecdiff(vector k, vector q){
	/* Returns the sum of two vectors */
	vector diff;
	diff.x = k.x - q.x;
	diff.y = k.y - q.y;
	diff.z = k.z - q.z;

	return diff;
}

vector vecscale(vector k, double scale){
	/* Returns the sum of two vectors */
	vector s;
	s.x = k.x*scale ;
	s.y = k.y*scale ;
	s.z = k.z*scale ;

	return s;
}

double vecdot(vector k, vector q){
	/* Returns the dot product of two vectors */

	return (k.x*q.x + k.y*q.y + k.z*q.z);
}

bool chkveczero(vector k){
	/* Checks whether all components of k are zero */

	if ((k.x == 0) && (k.y == 0) && (k.z == 0)){
		return 1;
	}
	else{
		return 0;
	}
}

void BZfold(vector *k){
	/* Returns the k-vector folded into [-0.5, 0.5) */
	
	/* kx folding */
	while ( !((k->x < 0.5) && (k->x >= -0.5)) ){
		if (k->x >= 0.5){
			k->x = k->x - 1.0;
		}
		else if (k->x < -0.5){
			k->x = k->x + 1.0;
		}
	}

	/* ky folding */
	while ( !((k->y < 0.5) && (k->y >= -0.5)) ){
		if (k->y >= 0.5){
			k->y = k->y - 1.0;
		}
		else if (k->y < -0.5){
			k->y = k->y + 1.0;
		}
	}

	/* kz folding */
	while ( !((k->z < 0.5) && (k->z >= -0.5)) ){
		if (k->z >= 0.5){
			k->z = k->z - 1.0;
		}
		else if (k->z < -0.5){
			k->z = k->z + 1.0;
		}
	}
}

void BZfold_positive(vector *k){
	/* Returns the k-vector folded into [0, 1) */
	
	/* kx folding */
	while ( !((k->x < 1.0) && (k->x >= 0.0)) ){
		if (k->x >= 1.0){
			k->x = k->x - 1.0;
		}
		else if (k->x < 0.0){
			k->x = k->x + 1.0;
		}
	}

	/* ky folding */
	while ( !((k->y < 1.0) && (k->y >= 0.0)) ){
		if (k->y >= 1.0){
			k->y = k->y - 1.0;
		}
		else if (k->y < 0.0){
			k->y = k->y + 1.0;
		}
	}

	/* kz folding */
	while ( !((k->z < 1.0) && (k->z >= 0.0)) ){
		if (k->z >= 1.0){
			k->z = k->z - 1.0;
		}
		else if (k->z < 0.0){
			k->z = k->z + 1.0;
		}
	}
}

void IBZfold(vector *k){

	// Folds the vector into the irreducible Brillouin zone (IBZ).

	// Comparison function
	int cmp(const void *x, const void *y)
	{
		double xx = *(double*)x, yy = *(double*)y;
		if (xx < yy) return -1;
		if (xx > yy) return  1;
		return 0;
	}

	// Fold k into the first BZ
	BZfold(k);

	// Take advantage of symmetry to move into the IBZ.
	// All coordinates are nonnegative, and kx <= ky <= kz.
	double ktmp[3] = {fabs(k->x), fabs(k->y), fabs(k->z)};
	qsort (ktmp, 3, sizeof(double), cmp);
	k->x = ktmp[0];
	k->y = ktmp[1];
	k->z = ktmp[2];
}


double Ek(int nband, vector k){
	// Calculate the energy of an electron in band nband with momentum k.

	if (PARABOLIC_BANDS == 1){
		// Use Klimin's anisotropic parabolic band approximation
		// printf("Test\n");
		return Sq(HBAR*k.x*TPI/LATA)/(2.0*M[nband].x) 
			+ Sq(HBAR*k.y*TPI/LATA)/(2.0*M[nband].y) 
			+ Sq(HBAR*k.z*TPI/LATA)/(2.0*M[nband].z)
			+ SHIFTS[nband];

		// double t_d = T_DELTA *9.86960440108936 ; // t_delta times pi^2: see section 5 a) of collision_integral.pdf
		// double t_p = T_PI *9.86960440108936 ; // t_pi times pi^2: see section 5 a) of collision_integral.pdf

		// if (nband==0){
		// 	return t_d*k.x*k.x + t_p*k.y*k.y + t_p*k.z*k.z;
		// }
		// else if (nband==1){
		// 	return t_p*k.x*k.x + t_d*k.y*k.y + t_p*k.z*k.z;
		// }
		// else if (nband==2){
		// 	return t_p*k.x*k.x + t_p*k.y*k.y + t_d*k.z*k.z;
		// }
		// else{
		// 	printf("Band %i undefined\n", nband);
		// 	return 0;
		// }
	}
	else{
		// Use an interpolated first-principles band structure

		// vertex finding is written for coordinates in [0,1) so ensure the vector is in this zone.
		BZfold_positive(&k);

		// Find the vertices of the cube in which k lies.
		// These are integer coordinates in an NINTERP^3 grid.
		int n = NINTERP;
		double ninv = 1.0/n;
		int vertices[8][3] = {
			{ (int)(floor(k.x*n)+0) % n , (int)(floor(k.y*n)+0) % n , (int)(floor(k.z*n)+0) % n  },
			{ (int)(floor(k.x*n)+0) % n , (int)(floor(k.y*n)+0) % n , (int)(floor(k.z*n)+1) % n  },
			{ (int)(floor(k.x*n)+0) % n , (int)(floor(k.y*n)+1) % n , (int)(floor(k.z*n)+0) % n  },
			{ (int)(floor(k.x*n)+0) % n , (int)(floor(k.y*n)+1) % n , (int)(floor(k.z*n)+1) % n  },
			{ (int)(floor(k.x*n)+1) % n , (int)(floor(k.y*n)+0) % n , (int)(floor(k.z*n)+0) % n  },
			{ (int)(floor(k.x*n)+1) % n , (int)(floor(k.y*n)+0) % n , (int)(floor(k.z*n)+1) % n  },
			{ (int)(floor(k.x*n)+1) % n , (int)(floor(k.y*n)+1) % n , (int)(floor(k.z*n)+0) % n  },
			{ (int)(floor(k.x*n)+1) % n , (int)(floor(k.y*n)+1) % n , (int)(floor(k.z*n)+1) % n  }
		};

		int i, ind;
		double energies[8];
		double k0[3];

		for (i=0; i<8; i++){
			// An ordering has been chosen so that the index of a point in the full zone depends simply on 
			// its integer coordinates:
			ind = vertices[i][2] + vertices[i][1]*n + vertices[i][0]*n*n;

			if ( (ind<0) || (ind >= NKBZ) ){
				// Sanity check.  This should never happen.
				printf("WARNING: Index %i out of range!\n", ind);
				return 0;
			}

			// IBZmap is a precomputed map between the full zone (ordering as chosen above) and the irreducible zone.
			// Look it up (and adjust: IBZmap was computed in fortran so it indexes from 1)
			ind = IBZmap[ind]-1;

			// Find the energy of the vertex
			energies[i] = cbands[nband][ind];
		}

		// Find the energy of k through trilinear interpolation
		// Notation as explained here: https://en.wikipedia.org/wiki/Trilinear_interpolation

		k0[0] = 1.0* vertices[0][0]*ninv;
		k0[1] = 1.0* vertices[0][1]*ninv;
		k0[2] = 1.0* vertices[0][2]*ninv;
		double xd = (k.x - k0[0]) * n;
		double yd = (k.y - k0[1]) * n;
		double zd = (k.z - k0[2]) * n;
		// printf("Difference to the corner: %f, %f, %f \n", xd, yd, zd);
		double c00 = energies[0]*(1.0-xd) + energies[4]*xd;
		double c10 = energies[2]*(1.0-xd) + energies[6]*xd;
		double c01 = energies[1]*(1.0-xd) + energies[5]*xd;
		double c11 = energies[3]*(1.0-xd) + energies[7]*xd;
		double c0  = c00*(1.0-yd) + c10*yd;
		double c1  = c01*(1.0-yd) + c11*yd;
		double energy = c0*(1.0-zd) + c1*zd;

		// Adjust so the CBM is at zero energy.
		return energy - 5.903665418;
	}

}

vector velocity_k(int n, vector k){

	if (PARABOLIC_BANDS==1){
		// Use the analytic form of the velocity
		double vx = HBAR / M[n].x * k.x * TPI/LATA;
		double vy = HBAR / M[n].y * k.y * TPI/LATA;
		double vz = HBAR / M[n].z * k.z * TPI/LATA;
		vector v = {vx, vy, vz};
		return v;
	}

	// Finds the band velocity of an electron in band nband with momentum k.
	// Numerically take the gradient of Ek and convert to the proper units
	
	double d = 1.0/NINTERP;
	double dinv = 0.5*NINTERP;  // 1/(2d) for the central difference derivative
	double conv = 2.41799e14*LATA; // 1/hbar / ((2pi)/a) = a/h.  Outputs velocity in A/s

	vector kxp = {k.x+ d , k.y+0.0, k.z+0.0};
	vector kxm = {k.x- d , k.y+0.0, k.z+0.0};
	vector kyp = {k.x+0.0, k.y+ d , k.z+0.0};
	vector kym = {k.x+0.0, k.y- d , k.z+0.0};
	vector kzp = {k.x+0.0, k.y+0.0, k.z+ d };
	vector kzm = {k.x+0.0, k.y+0.0, k.z- d };
	

	double vx = (Ek( n, kxp ) - Ek( n, kxm ))*dinv*conv;
	double vy = (Ek( n, kyp ) - Ek( n, kym ))*dinv*conv;
	double vz = (Ek( n, kzp ) - Ek( n, kzm ))*dinv*conv;
	vector v = {vx, vy, vz};

	return v;

}


double delta(double x){

	// Numerical approximation to the Dirac delta function
	// Gaussian with width SIGMA.

	double ainv = 1.0/SMEAR;
	double d = 0.5641895835477563*ainv*exp(-Sq(x)*Sq(ainv));
	return d;

}

double fermi(double energy){
	/* Fermi-Dirac distribution function */

	return 1.0/( exp((energy - MU)/(BOLTZMANNC * T)) + 1.0);

	// Returns zero past a certain cutoff so we don't try to keep track of very small doubles.
	// if (fabs(energy - MU)/(BOLTZMANNC * T) < 1e4){
	// 	return 1.0/( exp((energy - MU)/(BOLTZMANNC * T)) + 1.0);
	// }
	// else{
	// 	return 0.0;
	// }
}


double derv_fermi(double energy){
	/* Derviative of the Fermi-Dirac distribution w.r.t energy */

	return -1.0/(4.0 * BOLTZMANNC*T * Sq(cosh((energy - MU)/(2.0*BOLTZMANNC*T))) );
	
}
