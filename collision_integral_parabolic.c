/*
  demo-c.c
    test program for the Cuba library
    last modified 13 Mar 15 th
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "parse.h"
#include "module.h"
#include "find_mu.h"
// #include <mpi.h>

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

double kappa_s_sq; // Screening length
vector uvec; //  u vector for calculating Phi

vector band_masses[3];

typedef struct
{
  // integral_params: a type of struct which holds bands for the integrand.
  // In principle this could hold other parameters.
  int n1;
  int n2;
  int n3;
  int n4;
}integral_params;


double Ueff(int n1, vector k1, int n2, vector k2){
  // The effective Coulomb interaction between electrons
  // Explained in collision_integral.pdf

  vector qvec = vecdiff(k1, k2); // k1-k2
  double q = vecmag(qvec)*TPI/LATA; // magnitude of momentum transfer in A^-1

  double op_factor(void){
    // Finds the optical phonon factor

    int i, j;
    double num, denom;

    // LO phonon factor based on sum and products over modes
    // For more information, see collision_integral.pf section 1 b)

    double sum=0.0;
    for( i = 1; i <= ENLO[0]; i++){ // loop over polar LO mode only for scattering
      num = 1.0;
      denom = 1.0;
      for( j = 1; j <=ENTO[0]; j++){ // TO mode
        num = num*(1.0 - (ENTO[j]*ENTO[j])/(ENLO[i]*ENLO[i]) );
      }
      for( j = 1; j <=ENLO[0]; j++){ // polar LO mode
        if (j != i){
          denom = denom*(1.0 - (ENLO[j]*ENLO[j])/(ENLO[i]*ENLO[i]) );
        }
      }
      sum += fabs(num/denom); // For now, ignoring omega_{LO,i} / omega_{q,i}
  
    }

    // Put the prefactor and q dependance together with the sum to get the result.
    return -sum * ( 180.95125454 * q*q ) / (EPSI * (q*q + kappa_s_sq)*(q*q + kappa_s_sq));
  }

  double U_C  = 0.0; // Set to zero unless we include them
  double U_op = 0.0; // Set to zero unless we include them
  double U_ap = 0.0; // Set to zero unless we include them

  // Coulomb factor
  if (INCLUDE_UC != 0) U_C = 180.95125454/( (q*q + kappa_s_sq) * EPSI); // eV A^3

  // Optical phonon factor
  if (INCLUDE_OP != 0) U_op = op_factor();

  // Acoustic phonon factor
  if (INCLUDE_AP != 0) U_ap = -0.477881 * E_D * E_D; // eV A^3
  
  return U_C + U_op + U_ap;

}

static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *userdata) {

  // Integrand for the main collision integral

  ff[0] = 0.0;

  // Import the bands
  integral_params *bands = (integral_params*)userdata;
  int n1 = bands->n1;
  int n2 = bands->n2;
  int n3 = bands->n3;
  int n4 = bands->n4;
  vector t1 = band_masses[n1];
  vector t2 = band_masses[n2];
  vector t3 = band_masses[n3];
  vector t4 = band_masses[n4];

  // Get the k's that hold for either zero of the delta function.
  vector k1 = {xx[0]-0.5, xx[1]-0.5, xx[2]-0.5};
  vector k2 = {xx[3]-0.5, xx[4]-0.5, xx[5]-0.5};
  vector k3, k4;
  k3.x = xx[6]-0.5;
  k3.y = xx[7]-0.5;
  k4.x=k1.x+k2.x-k3.x;
  k4.y=k1.y+k2.y-k3.y;

  if(fabs(k4.x)>0.5 || fabs(k4.y>0.5)){
    // Don't currently support Umklapp
    return 0;
  }

  // Calculate energies and velocities that hold for either zero

  double e1 = Ek(n1,k1);
  double e2 = Ek(n2,k2);
  vector v1 = velocity_k(n1,k1);
  vector v2 = velocity_k(n2,k2);

  // Declare variables that change between the zeros
  double e3, e4, vfactor, U, fweights, deltafactor;
  vector v3, v4;

  // First zero:

  k3.z=(-2*k1.z*t4.z - 2*k2.z*t4.z - sqrt(pow(2*k1.z*t4.z + 2*k2.z*t4.z,2) - 
       4*(-t3.z - t4.z)*(k1.x*k1.x*t1.x + k1.y*k1.y*t1.y + k1.z*k1.z*t1.z + 
          k2.x*k2.x*t2.x + k2.y*k2.y*t2.y + k2.z*k2.z*t2.z - k3.x*k3.x*t3.x - 
          k3.y*k3.y*t3.y - k4.x*k4.x*t4.x - k4.y*k4.y*t4.y - k1.z*k1.z*t4.z - 
          2*k1.z*k2.z*t4.z - k2.z*k2.z*t4.z)))/(2.*(-t3.z - t4.z));
  k4.z=k1.z + k2.z + (k1.z*t4.z)/(-t3.z - t4.z) + (k2.z*t4.z)/(-t3.z - t4.z) + 
   sqrt(pow(2*k1.z*t4.z + 2*k2.z*t4.z,2) - 
      4*(-t3.z - t4.z)*(k1.x*k1.x*t1.x + k1.y*k1.y*t1.y + k1.z*k1.z*t1.z + 
         k2.x*k2.x*t2.x + k2.y*k2.y*t2.y + k2.z*k2.z*t2.z - k3.x*k3.x*t3.x - 
         k3.y*k3.y*t3.y - k4.x*k4.x*t4.x - k4.y*k4.y*t4.y - k1.z*k1.z*t4.z - 
         2*k1.z*k2.z*t4.z - k2.z*k2.z*t4.z))/(2.*(-t3.z - t4.z));


  if (k3.z==k3.z && k4.z==k4.z && fabs(k3.z)<=0.5 && fabs(k4.z)<= 0.5){
    // If the square root has a negative argument this is not a real solution: we get NaNs.
    // If the square roots have positive argument this is a real solution.
    // It is still skipped if the resulting k is outsize the first BZ.
    // Umklapp is unsupported.
    // Getting here means k3 and k4 are real, and inside the first BZ

    // Calculate the energies
    
    e3 = Ek(n3,k3);
    e4 = Ek(n4,k4);

    // This should be zero.  Check
    if (e1+e2-e3-e4 > 1e-14){
      printf("WARNING: e1+e2-e3-e4=%e is larger than expected!\n", e1+e2-e3-e4);
    }

    // Calculate the velocities
    v3 = velocity_k(n3,k3);
    v4 = velocity_k(n4,k4);

    // velocity factor:
    vfactor = vecdot(  vecdiff( vecsum(v1, v2), vecsum(v3,v4) ),  uvec  );

    // U factor:
    U = Ueff(n1, k1, n3, k3);

    // Fermi weights:
    fweights = fermi(e1)*fermi(e2)*(1.0-fermi(e3))*(1.0-fermi(e4));

    // Delta function now done analytically: divide by the derivative of the argument
    deltafactor = 1.0/fabs(2.0*t3.z*k3.z);

    ff[0] += vfactor*vfactor * U*U * fweights * deltafactor;
    // Units: Phi^2 * eV^2 A^6 * 1 * eV^-1 = eV Phi^2 A^6
  }

  // Now do the second zero of the delta function
  k3.z=(-2*k1.z*t4.z - 2*k2.z*t4.z + sqrt(pow(2*k1.z*t4.z + 2*k2.z*t4.z,2) - 
       4*(-t3.z - t4.z)*(k1.x*k1.x*t1.x + k1.y*k1.y*t1.y + k1.z*k1.z*t1.z + 
          k2.x*k2.x*t2.x + k2.y*k2.y*t2.y + k2.z*k2.z*t2.z - k3.x*k3.x*t3.x - 
          k3.y*k3.y*t3.y - k4.x*k4.x*t4.x - k4.y*k4.y*t4.y - k1.z*k1.z*t4.z - 
          2*k1.z*k2.z*t4.z - k2.z*k2.z*t4.z)))/(2.*(-t3.z - t4.z));
  k4.z=k1.z + k2.z + (k1.z*t4.z)/(-t3.z - t4.z) + (k2.z*t4.z)/(-t3.z - t4.z) - 
   sqrt(pow(2*k1.z*t4.z + 2*k2.z*t4.z,2) - 
      4*(-t3.z - t4.z)*(k1.x*k1.x*t1.x + k1.y*k1.y*t1.y + k1.z*k1.z*t1.z + 
         k2.x*k2.x*t2.x + k2.y*k2.y*t2.y + k2.z*k2.z*t2.z - k3.x*k3.x*t3.x - 
         k3.y*k3.y*t3.y - k4.x*k4.x*t4.x - k4.y*k4.y*t4.y - k1.z*k1.z*t4.z - 
         2*k1.z*k2.z*t4.z - k2.z*k2.z*t4.z))/(2.*(-t3.z - t4.z));

  if (k3.z==k3.z && k4.z==k4.z && fabs(k3.z)<=0.5 && fabs(k4.z)<= 0.5 ){
    // If the square root has a negative argument this is not a real solution: we get NaNs.
    // If the square roots have positive argument this is a real solution.
    // It is still skipped if the resulting k is outsize the first BZ.
    // Umklapp is unsupported.
    // Getting here means k3 and k4 are real, and inside the first BZ



    // Calculate the energies
    e3 = Ek(n3,k3);
    e4 = Ek(n4,k4);

    // This should be zero.  Check
    if (e1+e2-e3-e4 > 1e-14){
      printf("WARNING: e1+e2-e3-e4=%e is larger than expected!\n", e1+e2-e3-e4);
    }

    // Calculate the velocities
    v3 = velocity_k(n3,k3);
    v4 = velocity_k(n4,k4);

    // velocity factor:
    vfactor = vecdot(  vecdiff( vecsum(v1, v2), vecsum(v3,v4) ),  uvec  );

    // U factor:
    U = Ueff(n1, k1, n3, k3);

    // Fermi weights:
    fweights = fermi(e1)*fermi(e2)*(1.0-fermi(e3))*(1.0-fermi(e4));

    // Delta function now done analytically: divide by the derivative of the argument
    deltafactor = 1.0/fabs(2.0*t3.z*k3.z);

    ff[0] += vfactor*vfactor * U*U * fweights * deltafactor;
    // Units: Phi^2 * eV^2 A^6 * 1 * eV^-1 = eV Phi^2 A^6
  }

  return 0;
}

/*********************************************************************/

// Parameters for the Cuba integration

#define NDIM 8
#define NCOMP 1
#define NVEC 1
#define EPSREL 1e-3
#define EPSABS 1e-12
// #define VERBOSE 1
#define LAST 4
#define SEED 0
#define MINEVAL 0
// #define MAXEVAL 1e10

// #define NSTART 1000
// #define NINCREASE 500
// #define NBATCH 1000
// #define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

// #define NNEW 1000
// #define NMIN 2
// #define FLATNESS 25.

#define KEY1 1e4
#define KEY2 5
#define KEY3 1
#define MAXPASS 10
#define BORDER 0.
#define MAXCHISQ 10.
#define MINDEVIATION .25
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0


int main( int argc, char *argv[] ) {

  printf("++++++++++++++++++++++++++++++++++++++++\n");
  printf("+ Electron-Electron Collision Integral +\n");
  printf("+            Michael Swift             +\n");
  printf("+        Karthik Krishnaswamy          +\n");
  printf("+          Burak Himmetoglu            +\n");
  printf("++++++++++++++++++++++++++++++++++++++++\n\n");
  char input_file[] = "INPUT++++++++++++";
  if (argc >= 2){
    strcpy(input_file, argv[1]);
  }
  else{
    strcpy(input_file, "INPUT");
  }
  printf("Reading input file %s \n", input_file);
  parse_input(input_file);
  printf("Input parameters:\n");
  printf("Temperature %f K\n", T);
  printf("Electron density %e cm^-3\n", ELDEN);
  printf("Epsilon infinity %f\n", EPSI);
  printf("Deformation potential %f\n", E_D);
  printf("Delta function smearing %f eV\n", SMEAR);
  printf("Lattice parameter %f A\n", LATA);
  uvec.x = UVEC[0];
  uvec.y = UVEC[1];
  uvec.z = UVEC[2];
  printf("u vector for calculating Phi: (%f, %f, %f)\n", uvec.x, uvec.y, uvec.z);
  printf("TO Phonon Modes:\n");
  int i;
  for( i = 1; i <= ENTO[0]; i++){
    printf("    %f \n", ENTO[i]);}
  printf("LO Phonon Modes:\n");
  for( i = 1; i <= ENLO[0]; i++){
    printf("    %f \n", ENLO[i]);}

  PARABOLIC_BANDS = 1;
  printf("Using parabolic bands and an exact delta function\n");

  // set up the band parameters
  double t_d = 0.035 *9.86960440108936 ; // t_delta times pi^2: see section 5 a) of collision_integral.pdf
  double t_p = 0.615 *9.86960440108936 ; // t_pi times pi^2: see section 5 a) of collision_integral.pdf
  band_masses[0].x = t_d;
  band_masses[0].y = t_p;
  band_masses[0].z = t_p;

  band_masses[1].x = t_p;
  band_masses[1].y = t_d;
  band_masses[1].z = t_p;
    
  band_masses[2].x = t_p;
  band_masses[2].y = t_p;
  band_masses[2].z = t_d;

  if (PARABOLIC_BANDS == 1){
    printf("Using parabolic bands.\n");
  }
  else{
    printf("Using an interpolated first-principles band structure.\n");
  }

  printf("Integral includes:\n");
  if (INCLUDE_UC != 0){
    printf("    Coulomb interaction\n");
  }
  if (INCLUDE_OP != 0){
    printf("    LO phonon-mediated interaction\n");
  }
  if (INCLUDE_AP != 0){
    printf("    Acoustic phonon-mediated interaction\n");
  }

  int comp, nregions, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];


  printf("Finding chemical potential\n");
  MU = find_mu(ELDEN,&kappa_s_sq);
  
  double prefactor = 5.538743629461e19 / (T * pow(LATA,9)); // eV^-2 s^-1 A^-9

  integral_params bands = {BANDS[0], BANDS[1], BANDS[2], BANDS[3]};
  integral_params *userdata = &bands;

  printf("\nPerforming collision integral for bands (%i, %i, %i, %i).\n", bands.n1, bands.n2, bands.n3, bands.n4);

  printf("\n------------------- Divonne -------------------\n");

  long long int MAXEVAL = 1e10;
  long long int neval;

  llDivonne(NDIM, NCOMP, Integrand, userdata, NVEC,
    EPSREL, EPSABS, VERBOSE, SEED,
    MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
    BORDER, MAXCHISQ, MINDEVIATION,
    NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
    STATEFILE, SPIN,
    &nregions, &neval, &fail, integral, error, prob);

  printf("DIVONNE RESULT:\tnregions %d\tneval %lld\tfail %d\n",
    nregions, neval, fail);
  for( comp = 0; comp < NCOMP; ++comp )
    printf("DIVONNE RESULT:\t%.8e +- %.8e\tp = %.3f\n",
      (double)integral[comp], (double)error[comp], (double)prob[comp]);

  // Convert to the correct units
  double coll_int = prefactor*integral[0];
  // Units: (eV^-2 s^-1 A^-9) * (eV Phi^2 A^6) = eV^-1 s^-1 A^-3 Phi^2
  double coll_int_error = prefactor*error[0];

  printf("\nCollision integral:\t%.8e +- %.8e eV^-1 s^-1 A^-3 Phi^2\n",
      coll_int, coll_int_error);

  return 0;
}
