/*
  demo-c.c
    test program for the Cuba library
    last modified 13 Mar 15 th
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "parse.h"
#include "module.h"

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif


static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *params) {

  double e = *(double*) params;
  vector k = {.x = xx[0]-0.5, .y = xx[1]-0.5, .z = xx[2]-0.5};
  
  ff[0] = 0.0;
  int iband;
  for (iband=0; iband<NBANDS; iband++){
    double energy = Ek(iband, k);
    if (energy < e){
      ff[0]+=1;
    }
  }
  
  return 0;
}

/*********************************************************************/

#define NDIM 3
#define NCOMP 1
#define NVEC 1
#define EPSREL 1e-4
#define EPSABS 1e-12
// #define VERBOSE 1
#define LAST 4
#define SEED 0
#define MINEVAL 0
#define MAXEVAL 2e9

#define NSTART 1000
#define NINCREASE 500
#define NBATCH 1000
#define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

#define NNEW 1000
#define NMIN 2
#define FLATNESS 25.

#define KEY1 1e3
#define KEY2 5
#define KEY3 1
#define MAXPASS 10
#define BORDER 0.
#define MAXCHISQ 5.
#define MINDEVIATION .05
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0

int main(double elden_in, double *derivative) {
  parse_input("INPUT");

  int nregions, neval, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  cubareal integrate(double a){
    
    Divonne(NDIM, NCOMP, Integrand, &a, NVEC,
        EPSREL, EPSABS, VERBOSE_FIND_MU, SEED,
        MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
        BORDER, MAXCHISQ, MINDEVIATION,
        NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
        STATEFILE, SPIN,
        &nregions, &neval, &fail, integral, error, prob);

    // if (prob[0] > 0.1){
    //   printf("WARNING: Error estimate is incorrect with probability %f.  Retrying.\n", prob[0]);
    //   // integrate(a);
    // }
    return integral[0];
  }

  read_cbands();
  read_IBZmap();


  double emin = 0.0;
  double emax = 0.1;
  int samples = 1e2;

  double e = emin;
  float de = (emax-emin)/samples;
  cubareal Om_m1 = integrate(e-de);
  // printf("energy: %.4e, result %.4e \n", e-de, Om_m1);
  cubareal Om_0 = integrate(e);
  // printf("energy: %.4e, result %.4e \n", e, Om_0);
  cubareal Om_p1;

  printf("E       DOS \n");
  while (e <= emax){
    // Find the value one step ahead
    Om_p1 = integrate(e+de);
    // printf("energy: %.4e, result %.4e \n", e+de, Om_p1);
    // Calculate and print the derivative at e
    printf("%.4f  %.3e \n", e, (Om_p1-Om_m1)/(2.0*de) );
    // Update the values for the next point
    Om_m1 = Om_0;
    Om_0 = Om_p1;
    e += de;
  }

  return 0;
}

