/*
  demo-c.c
    test program for the Cuba library
    last modified 13 Mar 15 th
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "parse.h"
#include "module.h"

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

void bisection (double *x, double a, double b, int *itr)
  /* this function performs and prints the result of one iteration */
  {
      *x=(a+b)/2;
      ++(*itr);
      // printf("Iteration no. %3d X = %7.5f\n", *itr, *x);
}


static int Integrand(const int *ndim, const cubareal xx[],
  const int *ncomp, cubareal ff[], void *params) {
  // This is the integrand for the DOS integral which is used to find electron density

  double mu = *(double*) params;
  // vector k = {.x = xx[0], .y = xx[1], .z = xx[2]};
  vector k = {.x = xx[0]-0.5, .y = xx[1]-0.5, .z = xx[2]-0.5};
  int iband;
  double occ = 0.0;

  for (iband=0; iband<NBANDS; iband++){
    double energy = Ek(iband, k);
    occ += 2.0/( exp((energy - mu)/(BOLTZMANNC * T)) + 1.0);
  }
  ff[0] = occ;
  return 0;
}

/*********************************************************************/

#define NDIM 3
#define NCOMP 1
#define NVEC 1
#define EPSREL 1e-4
#define EPSABS 1e-12
// #define VERBOSE 1
#define LAST 4
#define SEED 0
#define MINEVAL 0
#define MAXEVAL 2e9

#define NSTART 1000
#define NINCREASE 500
#define NBATCH 1000
#define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

#define NNEW 1000
#define NMIN 2
#define FLATNESS 25.

#define KEY1 1e4
#define KEY2 5
#define KEY3 1
#define MAXPASS 10
#define BORDER 0.
#define MAXCHISQ 3.
#define MINDEVIATION .03
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0

double find_mu(double elden_in, double *kappa_s_sq) {

  if (ZERO_T_EF==1){
    double mD = pow(M[0].x*M[0].y*M[0].z,0.333333333);
    double mu = Sq(HBAR)/(2.0*mD) * pow(PI*PI*1.0e-24*elden_in,0.666666667);
    printf("Zero-T chemical potential: %f eV\n", mu);
    *kappa_s_sq = 6.0*PI*ESQ*1e-24*elden_in / (EPSI*mu);
    printf("Thomas-Fermi screening length: %f A\n", pow(*kappa_s_sq,-0.5));
    return mu;
  }

  int nregions, neval, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  void integrate(double *a){
    // performs the DOS integral to find the electron density at mu = a
    // Integration routine depends on METHOD_FINE_MU flag
    // VEGAS 0
    // SUAVE 1
    // DIVONNE 2 (Default)
    // Cuhre 3 

    if (METHOD_FIND_MU == 0){
      Vegas(NDIM, NCOMP, Integrand, a, NVEC,
        EPSREL, EPSABS, VERBOSE_FIND_MU, SEED,
        MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH,
        GRIDNO, STATEFILE, SPIN,
        &neval, &fail, integral, error, prob);
    }
    else if (METHOD_FIND_MU == 1){
      Suave(NDIM, NCOMP, Integrand, a, NVEC,
        EPSREL, EPSABS, VERBOSE_FIND_MU | LAST, SEED,
        MINEVAL, MAXEVAL, NNEW, NMIN, FLATNESS,
        STATEFILE, SPIN,
        &nregions, &neval, &fail, integral, error, prob);
    }
    else if (METHOD_FIND_MU == 3){
      Cuhre(NDIM, NCOMP, Integrand, a, NVEC,
        EPSREL, EPSABS, VERBOSE_FIND_MU | LAST,
        MINEVAL, MAXEVAL, KEY,
        STATEFILE, SPIN,
        &nregions, &neval, &fail, integral, error, prob);

    }
    else {
      Divonne(NDIM, NCOMP, Integrand, a, NVEC,
        EPSREL, EPSABS, VERBOSE_FIND_MU, SEED,
        MINEVAL, MAXEVAL, KEY1, KEY2, KEY3, MAXPASS,
        BORDER, MAXCHISQ, MINDEVIATION,
        NGIVEN, LDXGIVEN, NULL, NEXTRA, NULL,
        STATEFILE, SPIN,
        &nregions, &neval, &fail, integral, error, prob);
    }
  }

  // convert electrons per cm^3 to electrons per unit cell
  double target = elden_in * 1.0e-24 * LATA*LATA*LATA;

  printf("Desired density: %e, corresponding to %.8e electrons per unit cell.\n", elden_in, target);
  if (METHOD_FIND_MU == 0){
    printf("Finding mu with Vegas.\n");
  }
  else if (METHOD_FIND_MU == 1){
    printf("Finding mu with Suave.\n");
  }
  else if (METHOD_FIND_MU == 3){
    printf("Finding mu with Cuhre.\n");
  }
  else {
    printf("Finding mu with Divonne.\n");
  }

  int itr = 0, maxmitr;
  double x, a, b, allerr, x1, da, db, dx;
  a=MU_LBOUND;
  b=MU_UBOUND;

  // These parameters for finding mu are hardcoded
  allerr = 1e-6;
  maxmitr = 1e3;

  // Check if bisection will work, provides starting points
  integrate(&a);
  if (fail != 0){
    printf("WARNING: Integration failed!\n");
  }
  printf("Mu = %f, density = %.8e +- %.8e\tp = %.3f\n", a, integral[0], error[0], prob[0]);
  da = integral[0] - target;

  integrate(&b);
  if (fail != 0){
    printf("WARNING: Integration failed!\n");
  }
  printf("Mu = %f, density = %.8e +- %.8e\tp = %.3f\n", b, integral[0], error[0], prob[0]);
  db = integral[0] - target;

  if (da*db > 0.0){
    printf("Endpoints have the same sign.  Bisection will fail!\n");
    exit(1);
  }

  // The bisection routine itself
  printf("Beginning bisection\n");
  
  bisection (&x, a, b, &itr);
  integrate(&x);
  if (fail != 0){
    printf("WARNING: Integration failed!\n");
  }
  dx = integral[0] - target;
  do
  {

    printf("Mu = %f, density = %.8e +- %.8e\tp = %.3f\n", x, integral[0], error[0], prob[0]);
      if (da*dx < 0){
          b=x;
          db = dx;
        }
      else{
          a=x;
          da = dx;
        }
      bisection (&x1, a, b, &itr);

      if (fabs(x1-x) < allerr)
      {
        // x1 is the root.  Report the result.
        printf("Bisection complete after %d iterations! \nFinal Mu = %.8f eV\n", itr, x1);
        double m = 2.0*HBAR*HBAR * PI*PI / (LATA*LATA*T_PI);
        printf("If this is an Na calculation: k_F = %f\n",  sqrt(2.0*m*x1)/HBAR);
        // We'd also like to compute the derivative at this point, for use in calculating the screening length
        // Pick endpoints 5% away (Perhaps a constant distance would be better?)
        a = x1 * 0.95;
        integrate(&a);
        printf("Mu = %f, density = %.8e +- %.8e\tp = %.3f\n", a, integral[0], error[0], prob[0]);
        da = integral[0] - target;
        b = x1 * 1.05;
        integrate(&b);
        printf("Mu = %f, density = %.8e +- %.8e\tp = %.3f\n", b, integral[0], error[0], prob[0]);

        // Compute the derivative
        db = integral[0] - target;
        *kappa_s_sq = 180.95125454 * (db - da)/ (b-a) / (LATA*LATA*LATA) / EPSI; // A^-2
        printf("Lindhard screening length: %f A\n", pow(*kappa_s_sq,-0.5));
                  
        return x1;
      }

      // If we get here, bisection hasn't finished yet.  Prepare the next step.
      x=x1;
      integrate(&x);
      if (fail != 0){
        printf("WARNING: Integration failed!\n");
      }
      dx = integral[0] - target;
  }
  while (itr < maxmitr);
  printf("The solution does not converge or iterations are not sufficient\n");
  return 0.0;

}

