/* 
Routine for parsing inputs
Written by Karthik K., Michael S.
*/

#include "parse.h"

/* ** PARAMETERS WITH DEFAULT VALUES ** */
double MASS=0.2; // effective mass
double T=300;
double LATA=3.8772095665109871; //lattice parameter
double MU=0.05; // chemical potential
double MU_LBOUND = -0.2;
double MU_UBOUND = 0.2;
double EMAX = 5.0;
double ELDEN=1.0e19; // electron density in cm-3
double EPSI=5.44; // epsilon_infinity
double EPS=20; // epsilon_static
double E_D=23.3; // Deformation potential

double SMEAR=0.1; // smearing for delta function

int PARABOLIC_BANDS=0; // whether or not to use parabolic bands.
double T_PI = 0.615; // t_pi in eV
double T_DELTA = 0.035;// t_delta in eV
double SHIFTS[10] = {0};
int ZERO_T_EF=0; // whether or not to use Klimin's expressions for mu and kappa_s_sq.
int PMU_APPROX = 0; // Whether to approximate p = p_mu

char CBANDS_FILE[]="data/cbands.dat"; // File which stores cbands
char EQKFIT_FILE[]="data/eqkfit.dat"; // File which stores eqkfit
char VKFIT_FILE[]="data/vkfit.dat"; // File which stores vkfit
int NBANDS = 3; // Number of bands
int BANDS[4] = {0,0,0,0}; // Set of bands we are calculating
double UMKLAPP[3] = {0.0, 0.0, 1.0}; // Umklapp vector
int NKIBZ = 3276; // Number of k-points in the IBZ

vector M[10];

int VERBOSE = 2; // Verbosity of the Cuba routine in the main integral
int VERBOSE_FIND_MU = 0; // Verbosity of the Cuba routine when finding mu

int INCLUDE_UC = 1; // Include direct Coulomb interaction?
int INCLUDE_OP = 1; // Include optical phonon-mediated interaction?
int INCLUDE_AP = 1; // Include acoustic phonon-mediated interaction?

double QSCR=NOTSET; // epsilon_static
double NDERVMU=NOTSET; // epsilon_static

double TAUADD=NOTSET; // additional tau
double TAUOUT=NOTSET; // kz value to output tau(kx,ky)
double TAUCONST=NOTSET; // constant tau for el-ph

double ETAOUT=NOTSET; // kz value to output dimensionless eta in impurity calcs

int NSIG=251; // number of grid points along x,y,z for sigma integration
int NTAU=251; // number of grid points along x,y,z for tau integration
int NTAUSPH=21; // number of grid points in spherical for tau integration
int NINTERP=50; // number of grid points along x,y,z for interpolation
int NKBZ = 125000;
int METHOD_FIND_MU = 2;

double IMPDEN[10*sizeof(double)]={0,NOTSET,NOTSET,NOTSET,NOTSET,NOTSET,NOTSET,NOTSET,NOTSET,NOTSET}; // density of impurity defects in cm-3
double IMPZ[10*sizeof(double)]={0,0,0,0,0,0,0,0,0,0}; // charge of impurity defect; 0 indicates neutral defect
// double UVEC[4*sizeof(double)] = {0.0, 0.0, 0.0, 0.0}; // u vector for Phi = v dot u.  Indexed from 1 to match Karthik's input parsing convention.
double UVEC[3] = {0.0, 0.0, 0.0};

double ENTO[10*sizeof(double)]={1,300}; // TO mode frequencies in cm-1
double ENLO[10*sizeof(double)]={1,300}; // LO mode frequencies in cm-1

double **cbands;
int *IBZmap;

void read_cbands(void){

	// Allocate memory for cbands
	// double **cbands;
	cbands = malloc(NBANDS * sizeof(double *));

	int i,j;
	for(i = 0; i < NBANDS; i++){
		cbands[i] = malloc(NKIBZ * sizeof(double));
		if(cbands[i] == NULL){
			fprintf(stderr, "out of memory\n");
			return;
		}
	}

	FILE *fp;

	fp = fopen( CBANDS_FILE, "r" );
	char buff[255];
	

	for (i=0; i<NBANDS; i++){
		for (j=0; j<NKIBZ; j++){
			fgets(buff, 255, (FILE*)fp);
			cbands[i][j] = atof(buff);
		}
	}

	int status = fclose( fp );
	if (status != 0)
		printf("Warning: there was a problem closing %s", CBANDS_FILE);

}

void free_cbands(void){

	int i;
	for(i = 0; i < NBANDS; i++){
		free(cbands[i]);
	}

	free(cbands);
	
}

void read_IBZmap(void){

	// int *IBZmap;
	IBZmap = malloc(NKBZ * sizeof(int));

	FILE *fp;

	fp = fopen( EQKFIT_FILE, "r" );
	char buff[255];
	
	int i;
	for (i=0; i<NKBZ; i++){
		fgets(buff, 255, (FILE*)fp);
		IBZmap[i] = atoi(buff);
	}


	int status = fclose( fp );
	if (status != 0)
		printf("Warning: there was a problem closing %s", CBANDS_FILE);
}


char *trimspace(char *str)
{
	char *end;
	
	// Trim leading space
	while(isspace(*str)) str++;
	
	if(*str == 0)  // All spaces?
		return str;
	
	// Trim trailing space
	end = str + strlen(str) - 1;
	while(end > str && isspace(*end)) end--;
	
	// Write new null terminator
	*(end+1) = 0;
	
	return str;
}

char * stripcomments(char *str)
{
	int i=0;
	while(str[i] != 0){
		if ((str[i] == '#') || (str[i] == '!')){
			str[i] = 0;
			return str;
		}
		i++;
	}
	return str;
}

char * get_key(char *str)
{
	int i=0;

	while(str[i] != 0){
		if (str[i] == '='){
			str[i] = 0;
			str = trimspace(str);
			return str;
		}
		str[i] = tolower(str[i]);
		i++;
	}
	str[0] = 0;
	return str;
}

char * get_args(char *str)
{
	int i=strlen(str)-1;


	while(str[i] != 0){
		if (str[i] == '='){
			str = str + i+1;
			str = trimspace(str);
			return str;
		}
		i--;
	}
	str[0] = 0;
	return str;
}

// Lookup a key and assign the corresponding variables 
int lookup(char *key, char *args){
	int m;
	if (strcmp(key,"ento") == 0){
		m = sscanf(args,"%lf %lf %lf %lf %lf %lf %lf %lf %lf",&ENTO[1],&ENTO[2],&ENTO[3], &ENTO[4], &ENTO[5], &ENTO[6], &ENTO[7], &ENTO[8],&ENTO[9]);
		ENTO[0] = m;
	}
	else if (strcmp(key,"enlo") == 0){
		m = sscanf(args,"%lf %lf %lf %lf %lf %lf %lf %lf %lf",&ENLO[1],&ENLO[2],&ENLO[3], &ENLO[4], &ENLO[5], &ENLO[6], &ENLO[7], &ENLO[8],&ENLO[9]);
		ENLO[0] = m;
	}
	else if (strcmp(key,"uvec") == 0){
		m = sscanf(args,"%lf %lf %lf",&UVEC[0],&UVEC[1],&UVEC[2]);
	}
	else if (strcmp(key,"bands") == 0){
		m = sscanf(args,"%i %i %i %i",&BANDS[0],&BANDS[1],&BANDS[2],&BANDS[3]);
	}
	else if (strcmp(key,"umklapp") == 0){
		m = sscanf(args,"%lf %lf %lf", &UMKLAPP[0], &UMKLAPP[1], &UMKLAPP[2]);
	}
	else if (strcmp(key,"mass") == 0){
		m = sscanf(args,"%lf",&MASS);
	}
	else if (strcmp(key,"smear") == 0){
		m = sscanf(args,"%lf",&SMEAR);
	}
	else if (strcmp(key,"nbands") == 0){
		m = sscanf(args,"%i",&NBANDS);
	}
	else if (strcmp(key,"parabolic_bands") == 0){
		m = sscanf(args,"%i",&PARABOLIC_BANDS);
	}
	else if (strcmp(key,"t_pi") == 0){
		m = sscanf(args,"%lf",&T_PI);
	}
	else if (strcmp(key,"t_delta") == 0){
		m = sscanf(args,"%lf",&T_DELTA);
	}
	else if (strcmp(key,"shifts") == 0){
		m = sscanf(args,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",&SHIFTS[0],&SHIFTS[1],&SHIFTS[2],&SHIFTS[3], &SHIFTS[4], &SHIFTS[5], &SHIFTS[6], &SHIFTS[7], &SHIFTS[8],&SHIFTS[9]);
	}
	else if (strcmp(key,"zero_t_ef") == 0){
		m = sscanf(args,"%i",&ZERO_T_EF);
	}
	else if (strcmp(key,"pmu_approx") == 0){
		m = sscanf(args,"%i",&PMU_APPROX);
	}
	else if ((strcmp(key,"temp") == 0) || (strcmp(key,"t") == 0)){
		m = sscanf(args,"%lf",&T);
	}
	else if ((strcmp(key,"lata") == 0) || (strcmp(key,"a") == 0)){
		m = sscanf(args,"%lf",&LATA);
	}
	else if ((strcmp(key,"chem_pot") == 0) || (strcmp(key,"mu") == 0)){
		m = sscanf(args,"%lf",&MU);
	}
	else if (strcmp(key,"mu_lbound") == 0){
		m = sscanf(args,"%lf",&MU_LBOUND);
	}
	else if (strcmp(key,"mu_ubound") == 0){
		m = sscanf(args,"%lf",&MU_UBOUND);
	}
	else if (strcmp(key,"emax") == 0){
		m = sscanf(args,"%lf",&EMAX);
	}
	else if (strcmp(key,"elden") == 0){
		m = sscanf(args,"%lf",&ELDEN);
	}
	else if (strcmp(key,"epsi") == 0){
		m = sscanf(args,"%lf",&EPSI);
	}
	else if (strcmp(key,"eps") == 0){
		m = sscanf(args,"%lf",&EPS);
	}
	else if (strcmp(key,"e_d") == 0){
		m = sscanf(args,"%lf",&E_D);
	}
	else if (strcmp(key,"impden") == 0){
		m = sscanf(args,"%lf %lf %lf %lf %lf %lf %lf %lf %lf",&IMPDEN[1],&IMPDEN[2],&IMPDEN[3], &IMPDEN[4], &IMPDEN[5], &IMPDEN[6], &IMPDEN[7], &IMPDEN[8],&IMPDEN[9]);
		IMPDEN[0] = m;
	}
	else if (strcmp(key,"impz") == 0){
		m = sscanf(args,"%lf %lf %lf %lf %lf %lf %lf %lf %lf",&IMPZ[1],&IMPZ[2],&IMPZ[3], &IMPZ[4], &IMPZ[5], &IMPZ[6], &IMPZ[7], &IMPZ[8],&IMPZ[9]);
		IMPZ[0] = m;
	}
	else if (strcmp(key,"tauconst") == 0){
		m = sscanf(args,"%lf",&TAUCONST);
	}
	else if (strcmp(key,"tauadd") == 0){
		m = sscanf(args,"%lf",&TAUADD);
	}
	else if (strcmp(key,"tauout") == 0){
		m = sscanf(args,"%lf",&TAUOUT);
	}
	else if (strcmp(key,"etaout") == 0){
		m = sscanf(args,"%lf",&ETAOUT);
	}
	else if (strcmp(key,"qscr") == 0){
		m = sscanf(args,"%lf",&QSCR);
	}
	else if (strcmp(key,"ndervmu") == 0){
		m = sscanf(args,"%lf",&NDERVMU);
	}
	else if (strcmp(key,"verbose") == 0){
		m = sscanf(args,"%i",&VERBOSE);
	}
	else if (strcmp(key,"method_find_mu") == 0){
		m = sscanf(args,"%i",&METHOD_FIND_MU);
	}
	else if (strcmp(key,"include_uc") == 0){
		m = sscanf(args,"%i",&INCLUDE_UC);
	}
	else if (strcmp(key,"include_op") == 0){
		m = sscanf(args,"%i",&INCLUDE_OP);
	}
	else if (strcmp(key,"include_ap") == 0){
		m = sscanf(args,"%i",&INCLUDE_AP);
	}
	else if (strcmp(key,"verbose_find_mu") == 0){
		m = sscanf(args,"%i",&VERBOSE_FIND_MU);
	}
	else{
		return 1;
	}

	return 0;
}

int parse_input(char *fname){
	FILE *input;
	char line[80];
	char *trimmed;
	char key[10];
	char args[80];
	char strout[sizeof "INPUT***********: file not found\n"];

	input = fopen(fname,"r");
	if (input != NULL){
		while (fgets(line,sizeof line,input) != NULL){
			// printf("%s", line);
			trimmed = stripcomments(line);
			trimmed = trimspace(trimmed);
			if (!((trimmed[0] == 0) || (trimmed[strlen(trimmed)-1] == '='))){
				strncpy(args, trimspace(get_args(trimmed)),80);
				strncpy(key, trimspace(get_key(trimmed)),20);
				if (lookup(key,args) == 1){
					printf("WARNING: Key |%s| not recognized\n", key);
					// exit(1);
				}
			}
		}
		fclose(input);
		
	}
	else{
		sprintf(strout,"%s: file not found\n",fname);
		perror(strout);
		printf("File %s not found.  Using default parameters.\n",fname);
	}
	
	return 0;
}
